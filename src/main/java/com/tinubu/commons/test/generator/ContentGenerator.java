/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.test.generator;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ContentGenerator {

   private final Random random = new Random();

   public Stream<Character> randomCharacterStream(char start, char finish) {
      return IntStream.generate(() -> start + random.nextInt(finish - start + 1)).mapToObj(i -> (char) i);
   }

   public Stream<Character> randomCharacterStream() {
      return randomCharacterStream(' ', '~');
   }

   public Stream<Character> randomCharacterStream(char[] samples) {
      return IntStream.generate(() -> samples[random.nextInt() % samples.length]).mapToObj(i -> (char) i);
   }

   public Stream<Character> randomAlphaStream(boolean upperCase) {
      if (upperCase) {
         return randomCharacterStream('A', 'Z');
      } else {
         return randomCharacterStream('a', 'z');
      }
   }

   public Stream<Character> randomAlphanumericalStream(boolean upperCase) {
      if (upperCase) {
         return randomCharacterStream(new char[] {
               '0',
               '1',
               '2',
               '3',
               '4',
               '5',
               '6',
               '7',
               '8',
               '9',
               'A',
               'B',
               'C',
               'D',
               'E',
               'F',
               'G',
               'H',
               'I',
               'J',
               'K',
               'L',
               'M',
               'N',
               'O',
               'P',
               'Q',
               'R',
               'S',
               'T',
               'U',
               'V',
               'W',
               'X',
               'Y',
               'Z' });
      } else {
         return randomCharacterStream(new char[] {
               '0',
               '1',
               '2',
               '3',
               '4',
               '5',
               '6',
               '7',
               '8',
               '9',
               'a',
               'b',
               'c',
               'd',
               'e',
               'f',
               'g',
               'h',
               'i',
               'j',
               'k',
               'l',
               'm',
               'n',
               'o',
               'p',
               'q',
               'r',
               's',
               't',
               'u',
               'v',
               'w',
               'x',
               'y',
               'z' });

      }
   }

   public Stream<Character> characterStream(char c) {
      return IntStream.generate(() -> c).mapToObj(i -> (char) i);
   }

   public Stream<Character> zeroCharacterStream() {
      return characterStream('0');
   }

   public Stream<Byte> randomByteStream(byte start, byte finish) {
      return IntStream.generate(() -> start + random.nextInt(finish - start + 1)).mapToObj(i -> (byte) i);
   }

   public Stream<Byte> randomByteStream() {
      return randomByteStream(Byte.MIN_VALUE, Byte.MAX_VALUE);
   }

   public Stream<Byte> byteStream(byte b) {
      return IntStream.generate(() -> b).mapToObj(i -> (byte) i);
   }

   public Stream<Byte> zeroByteStream() {
      return byteStream((byte) 0);
   }

}
