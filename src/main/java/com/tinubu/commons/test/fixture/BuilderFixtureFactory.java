package com.tinubu.commons.test.fixture;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

import com.tinubu.commons.lang.model.Builder;
import com.tinubu.commons.lang.util.Pair;

/**
 * An abstract fixture factory for objects using a {@link Builder}.
 * Supplementary methods are provided to generate built fixtures.
 *
 * @param <T> Domain object type
 * @param <B> Domain object builder type
 */
public abstract class BuilderFixtureFactory<T, B extends Builder<T>> extends DefaultFixtureFactory<B> {

   public T build(int index) throws IndexOutOfBoundsException {
      return create(index).build();
   }

   public T build() {
      return create().build();
   }

   public List<T> buildList(int count) {
      return list(stream(super.createList(count)).map(Builder::build));
   }

   public <V> Map<V, T> buildMap(int count, Function<? super T, ? extends V> keyIndexer) {
      notNull(keyIndexer, "keyIndexer");

      return buildList(count).stream().collect(toMap(keyIndexer, identity()));
   }

   public <V> Map<V, T> buildMap(int count, BiFunction<Integer, ? super T, ? extends V> keyIndexer) {
      notNull(keyIndexer, "keyIndexer");

      List<T> fixtures = buildList(count);

      return map(IntStream
                       .range(0, count)
                       .mapToObj(i -> Pair.of(keyIndexer.apply(i, fixtures.get(i)), fixtures.get(i))));
   }

}
