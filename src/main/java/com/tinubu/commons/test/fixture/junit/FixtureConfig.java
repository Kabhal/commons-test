package com.tinubu.commons.test.fixture.junit;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Fixture extension configuration.
 * Can be set at test class level or at test method (override class level).
 */
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface FixtureConfig {

   /**
    * Packages to scan for fixture factories.
    * Narrow the package value to fasten the scanning.
    *
    * @return default packages to scan
    */
   String[] basePackages() default "";

}
