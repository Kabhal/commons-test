package com.tinubu.commons.test.fixture.junit;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to put on JUnit test parameter to inject a fixture.
 */
@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Fixture {
   /**
    * Number of items to generate in collection. By default, generates a collection of one element.
    *
    * @return number of items to generate
    */
   int count() default 1;
}
