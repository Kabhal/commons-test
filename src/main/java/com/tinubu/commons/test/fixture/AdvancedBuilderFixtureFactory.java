package com.tinubu.commons.test.fixture;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import com.tinubu.commons.lang.model.AdvancedBuilder;
import com.tinubu.commons.lang.util.Pair;

/**
 * An abstract fixture factory for objects using an {@link AdvancedBuilder}.
 * Supplementary methods are provided to generate built fixtures.
 *
 * @param <T> Domain object type
 * @param <B> Domain object builder type
 */
public abstract class AdvancedBuilderFixtureFactory<T, B extends AdvancedBuilder<T>>
      extends BuilderFixtureFactory<T, B> {

   @SuppressWarnings("unchecked")
   public T build(Function<? super B, ? extends B> buildMapper) {
      notNull(buildMapper, "buildMapper");

      return create()
            .chain((Function<? super AdvancedBuilder<T>, ? extends AdvancedBuilder<T>>) buildMapper)
            .build();
   }

   @SuppressWarnings("unchecked")
   public T build(Predicate<? super B> buildMapperPredicate, Function<? super B, ? extends B> buildMapper) {
      notNull(buildMapperPredicate, "buildMapperPredicate");
      notNull(buildMapper, "buildMapper");

      return create()
            .conditionalChain((Predicate<? super AdvancedBuilder<T>>) buildMapperPredicate,
                              (Function<? super AdvancedBuilder<T>, ? extends AdvancedBuilder<T>>) buildMapper)
            .build();
   }

   @SuppressWarnings("unchecked")
   public T build(int index, Function<? super B, ? extends B> buildMapper) throws IndexOutOfBoundsException {
      notNull(buildMapper, "buildMapper");

      return create(index)
            .chain((Function<? super AdvancedBuilder<T>, ? extends AdvancedBuilder<T>>) buildMapper)
            .build();
   }

   @SuppressWarnings("unchecked")
   public T build(int index,
                  Predicate<? super B> buildMapperPredicate,
                  Function<? super B, ? extends B> buildMapper) throws IndexOutOfBoundsException {
      notNull(buildMapperPredicate, "buildMapperPredicate");
      notNull(buildMapper, "buildMapper");

      return create(index)
            .conditionalChain((Predicate<? super AdvancedBuilder<T>>) buildMapperPredicate,
                              (Function<? super AdvancedBuilder<T>, ? extends AdvancedBuilder<T>>) buildMapper)
            .build();
   }

   public List<T> buildList(int count, Function<? super B, ? extends B> buildMapper) {
      notNull(buildMapper, "buildMapper");

      return buildList(count, (__, b) -> buildMapper.apply(b));
   }

   @SuppressWarnings("unchecked")
   public List<T> buildList(int count,
                            Predicate<? super B> buildMapperPredicate,
                            Function<? super B, ? extends B> buildMapper) {
      notNull(buildMapperPredicate, "buildMapperPredicate");
      notNull(buildMapper, "buildMapper");

      List<B> fixtures = super.createList(count);

      return list(fixtures
                        .stream()
                        .map(fixture -> fixture
                              .conditionalChain(b -> buildMapperPredicate.test((B) b),
                                                b -> buildMapper.apply((B) b))
                              .build()));
   }

   @SuppressWarnings("unchecked")
   public List<T> buildList(int count, BiFunction<Integer, ? super B, ? extends B> buildMapper) {
      notNull(buildMapper, "buildMapper");

      List<B> fixtures = super.createList(count);

      return list(IntStream
                        .range(0, fixtures.size())
                        .mapToObj(i -> fixtures.get(i).chain(b -> buildMapper.apply(i, (B) b)).build()));
   }

   @SuppressWarnings("unchecked")
   public List<T> buildList(int count,
                            BiPredicate<Integer, ? super B> buildMapperPredicate,
                            BiFunction<Integer, ? super B, ? extends B> buildMapper) {
      notNull(buildMapperPredicate, "buildMapperPredicate");
      notNull(buildMapper, "buildMapper");

      List<B> fixtures = super.createList(count);

      return list(IntStream
                        .range(0, fixtures.size())
                        .mapToObj(i -> fixtures
                              .get(i)
                              .conditionalChain(b -> buildMapperPredicate.test(i, (B) b),
                                                b -> buildMapper.apply(i, (B) b))
                              .build()));
   }

   @SuppressWarnings("unchecked")
   public <V> Map<V, T> buildMap(int count,
                                 Function<? super T, ? extends V> keyIndexer,
                                 Function<? super B, ? extends B> buildMapper) {
      notNull(keyIndexer, "keyIndexer");
      notNull(buildMapper, "buildMapper");

      return buildList(count, buildMapper).stream().collect(toMap(keyIndexer, identity()));
   }

   @SuppressWarnings("unchecked")
   public <V> Map<V, T> buildMap(int count,
                                 Function<? super T, ? extends V> keyIndexer,
                                 Predicate<? super B> buildMapperPredicate,
                                 Function<? super B, ? extends B> buildMapper) {
      notNull(keyIndexer, "keyIndexer");
      notNull(buildMapperPredicate, "buildMapperPredicate");
      notNull(buildMapper, "buildMapper");

      return buildList(count, buildMapperPredicate, buildMapper)
            .stream()
            .collect(toMap(keyIndexer, identity()));
   }

   @SuppressWarnings("unchecked")
   public <V> Map<V, T> buildMap(int count,
                                 BiFunction<Integer, ? super T, ? extends V> keyIndexer,
                                 BiFunction<Integer, ? super B, ? extends B> buildMapper) {
      notNull(keyIndexer, "keyIndexer");
      notNull(buildMapper, "buildMapper");

      List<T> fixtures = buildList(count, buildMapper);

      return map(IntStream
                       .range(0, fixtures.size())
                       .mapToObj(i -> Pair.of(keyIndexer.apply(i, fixtures.get(i)), fixtures.get(i))));
   }

   @SuppressWarnings("unchecked")
   public <V> Map<V, T> buildMap(int count,
                                 BiFunction<Integer, ? super T, ? extends V> keyIndexer,
                                 BiPredicate<Integer, ? super B> buildMapperPredicate,
                                 BiFunction<Integer, ? super B, ? extends B> buildMapper) {
      notNull(keyIndexer, "keyIndexer");
      notNull(buildMapperPredicate, "buildMapperPredicate");
      notNull(buildMapper, "buildMapper");

      List<T> fixtures = buildList(count, buildMapperPredicate, buildMapper);

      return map(IntStream
                       .range(0, fixtures.size())
                       .mapToObj(i -> Pair.of(keyIndexer.apply(i, fixtures.get(i)), fixtures.get(i))));
   }

}
