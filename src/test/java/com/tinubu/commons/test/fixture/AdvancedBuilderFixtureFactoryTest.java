package com.tinubu.commons.test.fixture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.tinubu.commons.lang.model.Builder;
import com.tinubu.commons.test.fixture.junit.FixtureExtension;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectC;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectC.ObjectCBuilder;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectCFixtureFactory;

@ExtendWith(FixtureExtension.class)
class AdvancedBuilderFixtureFactoryTest {

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildWithMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().build(null))
            .withMessage("'buildMapper' must not be null");

      ObjectC testObject = new ObjectCFixtureFactory().build(b -> b.index(1));

      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(1);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildWithIndexAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().build(0, null))
            .withMessage("'buildMapper' must not be null");

      ObjectC testObject = new ObjectCFixtureFactory().build(0, b -> b.index(1));

      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(1);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildWithPredicateAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().build(null, b -> b.index(1)))
            .withMessage("'buildMapperPredicate' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().build(__ -> true, null))
            .withMessage("'buildMapper' must not be null");

      ObjectC testObject1 = new ObjectCFixtureFactory().build(__ -> true, b -> b.index(1));

      assertThat(testObject1).isNotNull();
      assertThat(testObject1.index()).isEqualTo(1);

      ObjectC testObject2 = new ObjectCFixtureFactory().build(__ -> false, b -> b.index(1));

      assertThat(testObject2).isNotNull();
      assertThat(testObject2.index()).isEqualTo(0);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildWithIndexPredicateAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().build(0, null, b -> b.index(1)))
            .withMessage("'buildMapperPredicate' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().build(0, __ -> true, null))
            .withMessage("'buildMapper' must not be null");

      ObjectC testObject1 = new ObjectCFixtureFactory().build(0, __ -> true, b -> b.index(1));

      assertThat(testObject1).isNotNull();
      assertThat(testObject1.index()).isEqualTo(1);

      ObjectC testObject2 = new ObjectCFixtureFactory().build(0, __ -> false, b -> b.index(1));

      assertThat(testObject2).isNotNull();
      assertThat(testObject2.index()).isEqualTo(0);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildListWithMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildList(1,
                                                                    (Function<? super ObjectCBuilder, ? extends ObjectCBuilder>) null))
            .withMessage("'buildMapper' must not be null");

      List<ObjectC> testObjects = new ObjectCFixtureFactory().buildList(1, b -> b.index(1));

      assertThat(testObjects).extracting(ObjectC::index).containsExactly(1);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildListWithPredicateAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildList(1, null, b -> b.index(1)))
            .withMessage("'buildMapperPredicate' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildList(1, __ -> true, null))
            .withMessage("'buildMapper' must not be null");

      List<ObjectC> testObjects1 = new ObjectCFixtureFactory().buildList(1, __ -> true, b -> b.index(1));

      assertThat(testObjects1).extracting(ObjectC::index).containsExactly(1);

      List<ObjectC> testObjects2 = new ObjectCFixtureFactory().buildList(1, __ -> false, b -> b.index(1));

      assertThat(testObjects2).isNotNull();
      assertThat(testObjects2).extracting(ObjectC::index).containsExactly(0);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildListWithIndexedMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildList(1,
                                                                    (BiFunction<Integer, ? super ObjectCBuilder, ? extends ObjectCBuilder>) null))
            .withMessage("'buildMapper' must not be null");

      List<ObjectC> testObjects = new ObjectCFixtureFactory().buildList(1, (i, b) -> b.index(1));

      assertThat(testObjects).extracting(ObjectC::index).containsExactly(1);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildListWithIndexedPredicateAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildList(1, null, (i, b) -> b.index(1)))
            .withMessage("'buildMapperPredicate' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildList(1, (i, __) -> true, null))
            .withMessage("'buildMapper' must not be null");

      List<ObjectC> testObjects1 =
            new ObjectCFixtureFactory().buildList(1, (i, __) -> true, (i, b) -> b.index(1));

      assertThat(testObjects1).extracting(ObjectC::index).containsExactly(1);

      List<ObjectC> testObjects2 =
            new ObjectCFixtureFactory().buildList(1, (i, __) -> false, (i, b) -> b.index(1));

      assertThat(testObjects2).isNotNull();
      assertThat(testObjects2).extracting(ObjectC::index).containsExactly(0);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildMapWithMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, null, b -> b.index(1)))
            .withMessage("'keyIndexer' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, ObjectC::index, null))
            .withMessage("'buildMapper' must not be null");

      AtomicInteger i = new AtomicInteger();
      Map<Integer, ObjectC> testObjects =
            new ObjectCFixtureFactory().buildMap(3, __ -> i.getAndIncrement(), b -> b.index(1));

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys(0, 1, 2);
      assertThat(testObjects.values()).extracting(ObjectC::index).containsExactly(1, 1, 1);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildMapWithPredicateAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, null, __ -> true, b -> b.index(1)))
            .withMessage("'keyIndexer' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, ObjectC::index, null, b -> b.index(1)))
            .withMessage("'buildMapperPredicate' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, ObjectC::index, __ -> true, null))
            .withMessage("'buildMapper' must not be null");

      AtomicInteger i1 = new AtomicInteger();
      Map<Integer, ObjectC> testObjects1 =
            new ObjectCFixtureFactory().buildMap(3, __ -> i1.getAndIncrement(), __ -> true, b -> b.index(1));

      assertThat(testObjects1).isNotNull();
      assertThat(testObjects1).containsKeys(0, 1, 2);
      assertThat(testObjects1.values()).extracting(ObjectC::index).containsExactly(1, 1, 1);

      AtomicInteger i2 = new AtomicInteger();
      Map<Integer, ObjectC> testObjects2 =
            new ObjectCFixtureFactory().buildMap(3, __ -> i2.getAndIncrement(), __ -> false, b -> b.index(1));

      assertThat(testObjects2).isNotNull();
      assertThat(testObjects2).containsKeys(0, 1, 2);
      assertThat(testObjects2.values()).extracting(ObjectC::index).containsExactly(0, 1, 2);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildMapWithIndexedMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, null, (i, b) -> b.index(1)))
            .withMessage("'keyIndexer' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, (i, o) -> o.index(), null))
            .withMessage("'buildMapper' must not be null");

      Map<Integer, ObjectC> testObjects =
            new ObjectCFixtureFactory().buildMap(3, (i, __) -> i, (i, b) -> b.index(1));

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys(0, 1, 2);
      assertThat(testObjects.values()).extracting(ObjectC::index).containsExactly(1, 1, 1);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenBuildMapWithIndexedPredicateAndMapper() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3,
                                                                   null,
                                                                   (i, __) -> true,
                                                                   (i, b) -> b.index(1)))
            .withMessage("'keyIndexer' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3,
                                                                   (i, o) -> o.index(),
                                                                   null,
                                                                   (i, b) -> b.index(1)))
            .withMessage("'buildMapperPredicate' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3,
                                                                   (i, o) -> o.index(),
                                                                   (i, __) -> true,
                                                                   null))
            .withMessage("'buildMapper' must not be null");

      Map<Integer, ObjectC> testObjects1 =
            new ObjectCFixtureFactory().buildMap(3, (i, __) -> i, (i, __) -> true, (i, b) -> b.index(1));

      assertThat(testObjects1).isNotNull();
      assertThat(testObjects1).containsKeys(0, 1, 2);
      assertThat(testObjects1.values()).extracting(ObjectC::index).containsExactly(1, 1, 1);

      Map<Integer, ObjectC> testObjects2 =
            new ObjectCFixtureFactory().buildMap(3, (i, __) -> i, (i, __) -> false, (i, b) -> b.index(1));

      assertThat(testObjects2).isNotNull();
      assertThat(testObjects2).containsKeys(0, 1, 2);
      assertThat(testObjects2.values()).extracting(ObjectC::index).containsExactly(0, 1, 2);
   }

   @Test
   public void testAdvancedBuilderFixtureFactoryWhenCovariantContravariantGenerics() {
      BiFunction<Integer, Object, String> indexer = (i, __) -> String.valueOf(i);
      BiPredicate<Integer, Object> predicate = (i, __) -> true;
      BiFunction<Integer, Builder<ObjectC>, ChildObjectCBuilder> mapper =
            (i, b) -> new ChildObjectCBuilder().index(1);

      Map<CharSequence, ObjectC> testObjects =
            new ObjectCFixtureFactory().buildMap(3, indexer, predicate, mapper);

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys("0", "1", "2");
      assertThat(testObjects.values()).extracting(ObjectC::index).containsExactly(1, 1, 1);
   }

   public static class ChildObjectCBuilder extends ObjectCBuilder {
      @Override
      public ChildObjectCBuilder index(int index) {
         return (ChildObjectCBuilder) super.index(index);
      }
   }

}