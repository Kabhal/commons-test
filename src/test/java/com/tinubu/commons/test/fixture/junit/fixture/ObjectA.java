package com.tinubu.commons.test.fixture.junit.fixture;

import com.tinubu.commons.lang.model.Builder;

public class ObjectA {

   private int index;

   public ObjectA(int index) {
      this.index = index;
   }

   public int index() {
      return index;
   }

   public static class ObjectABuilder implements Builder<ObjectA> {

      private int index = 0;

      public ObjectABuilder index(int index) {
         this.index = index;
         return this;
      }

      @Override
      public ObjectA build() {
         return new ObjectA(index);
      }
   }
}
