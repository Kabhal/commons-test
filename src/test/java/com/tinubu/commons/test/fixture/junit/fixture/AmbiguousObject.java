package com.tinubu.commons.test.fixture.junit.fixture;

public class AmbiguousObject {

   int index;

   public AmbiguousObject(int index) {
      this.index = index;
   }

   public int getIndex() {
      return index;
   }
}
