package com.tinubu.commons.test.fixture.junit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.type;
import static org.junit.platform.commons.util.ClassUtils.nullSafeToString;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectMethod;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.assertj.core.data.Index;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfSystemProperty;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.platform.commons.JUnitException;
import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.engine.discovery.MethodSelector;
import org.junit.platform.testkit.engine.EngineTestKit;
import org.junit.platform.testkit.engine.Event;
import org.junit.platform.testkit.engine.EventType;

import com.tinubu.commons.test.fixture.junit.fixture.AmbiguousObject;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectA;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectB;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectC;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectC.ObjectCBuilder;

@ExtendWith(FixtureExtension.class)
@FixtureConfig(basePackages = "com.tinubu.commons.test.fixture.junit.fixture")
class FixtureExtensionTest {

   /**
    * Tests extension internals. Extension injection failures can't be checked in normal tests as the test
    * can't be started successfully.
    * These tests are executed using Junit platform testkit and should be disabled
    * by default, we use a system property to enable them in testkit tests.
    */
   @Nested
   @DisabledIfSystemProperty(named = "extensionInternalTests", matches = "false")
   public class InjectionFailures {

      @BeforeEach
      public void beforeEach() {
         System.setProperty("extensionInternalTests", "false");
      }

      @Test
      public void testFixtureInjectionWhenUnknownFixture(@Fixture String testObject) {
      }

      @Test
      public void testFixtureInjectionWhenUnknownFixtureList(@Fixture List<String> testObject) {
      }

      @Test
      public void testFixtureInjectionWhenCustomList(@Fixture CustomList<ObjectA> testObjects) {
      }

      @Test
      public void testFixtureInjectionWhenAmbiguousFixtureFactory(@Fixture AmbiguousObject ambiguousObject) {
      }

   }

   @Test
   public void testFixtureInjectionWhenUnknownFixture() {
      System.setProperty("extensionInternalTests", "true");

      assertThatExtensionFailed(selectMethod(InjectionFailures.class,
                                             "testFixtureInjectionWhenUnknownFixture",
                                             nullSafeToString(String.class)),
                                ParameterResolutionException.class,
                                "No fixture factory found for object 'class java.lang.String' with '@com.tinubu.commons.test.fixture.junit.FixtureConfig(basePackages={\"com.tinubu.commons.test.fixture.junit.fixture\"})' config");
   }

   @Test
   public void testFixtureInjectionWhenUnknownFixtureList() {
      System.setProperty("extensionInternalTests", "true");

      assertThatExtensionFailed(selectMethod(InjectionFailures.class,
                                             "testFixtureInjectionWhenUnknownFixtureList",
                                             nullSafeToString(List.class)),
                                ParameterResolutionException.class,
                                "No fixture factory found for object 'class java.lang.String' with '@com.tinubu.commons.test.fixture.junit.FixtureConfig(basePackages={\"com.tinubu.commons.test.fixture.junit.fixture\"})' config");
   }

   @Test
   public void testFixtureInjectionWhenCustomList() {
      System.setProperty("extensionInternalTests", "true");

      assertThatExtensionFailed(selectMethod(InjectionFailures.class,
                                             "testFixtureInjectionWhenCustomList",
                                             nullSafeToString(CustomList.class)),
                                ParameterResolutionException.class,
                                "No fixture factory found for object 'class com.tinubu.commons.test.fixture.junit.FixtureExtensionTest$CustomList' with '@com.tinubu.commons.test.fixture.junit.FixtureConfig(basePackages={\"com.tinubu.commons.test.fixture.junit.fixture\"})' config");
   }

   @Test
   public void testFixtureInjectionWhenAmbiguousFixtureFactory() {
      System.setProperty("extensionInternalTests", "true");

      assertThatExtensionFailed(selectMethod(InjectionFailures.class,
                                             "testFixtureInjectionWhenAmbiguousFixtureFactory",
                                             nullSafeToString(AmbiguousObject.class)),
                                ParameterResolutionException.class,
                                "Found multiple fixture factories for 'class com.tinubu.commons.test.fixture.junit.fixture.AmbiguousObject' with '@com.tinubu.commons.test.fixture.junit.FixtureConfig(basePackages={\"com.tinubu.commons.test.fixture.junit.fixture\"})' config : com.tinubu.commons.test.fixture.junit.fixture.AmbiguousObject1FixtureFactory, com.tinubu.commons.test.fixture.junit.fixture.AmbiguousObject2FixtureFactory");
   }

   @BeforeAll
   public static void testFixtureInjectionWhenBeforeAll(@Fixture ObjectA testObject1,
                                                        @Fixture ObjectA testObject2) {
      assertThat(testObject1.index()).isEqualTo(0);
      assertThat(testObject2.index()).isEqualTo(1);
   }

   @BeforeEach
   public void testFixtureInjectionWhenBeforeEach(@Fixture ObjectA testObject1,
                                                  @Fixture ObjectA testObject2) {
      assertThat(testObject1.index()).isEqualTo(0);
      assertThat(testObject2.index()).isEqualTo(1);
   }

   @AfterEach
   public void testFixtureInjectionWhenAfterEach(@Fixture ObjectA testObject1, @Fixture ObjectA testObject2) {
      assertThat(testObject1.index()).isEqualTo(0);
      assertThat(testObject2.index()).isEqualTo(1);
   }

   @AfterAll
   public static void testFixtureInjectionWhenAfterAll(@Fixture ObjectA testObject1,
                                                       @Fixture ObjectA testObject2) {
      assertThat(testObject1.index()).isEqualTo(0);
      assertThat(testObject2.index()).isEqualTo(1);
   }

   @Test
   public void testFixtureInjectionWhenNominal(@Fixture ObjectA testObject) {
      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(0);
   }

   @Test
   public void testFixtureInjectionWhenList(@Fixture List<ObjectA> testObjects) {
      assertThat(testObjects).isNotNull();
      assertThat(testObjects).extracting(ObjectA::index).containsExactly(0);
   }

   @Test
   public void testFixtureInjectionWhenCollection(@Fixture Collection<ObjectA> testObjects) {
      assertThat(testObjects).isNotNull();
      assertThat(testObjects).extracting(ObjectA::index).containsExactly(0);
   }

   @Test
   public void testFixtureInjectionWhenListWithExplicitCount(@Fixture(count = 2) List<ObjectA> testObjects) {
      assertThat(testObjects).isNotNull();
      assertThat(testObjects).extracting(ObjectA::index).containsExactly(0, 1);
   }

   @Test
   public void testFixtureInjectionWhenSeveralFixtures(@Fixture ObjectA testObject1,
                                                       @Fixture ObjectA testObject2) {
      assertThat(testObject1).isNotNull();
      assertThat(testObject1.index()).isEqualTo(0);

      assertThat(testObject2).isNotNull();
      assertThat(testObject2.index()).isEqualTo(1);
   }

   @Test
   public void testFixtureInjectionWhenSeveralDifferentFixtures(@Fixture ObjectA testObjectA1,
                                                                @Fixture ObjectB testObjectB1,
                                                                @Fixture ObjectA testObjectA2,
                                                                @Fixture ObjectB testObjectB2) {
      assertThat(testObjectA1).isNotNull();
      assertThat(testObjectA1.index()).isEqualTo(0);

      assertThat(testObjectB1).isNotNull();
      assertThat(testObjectB1.index()).isEqualTo(0);

      assertThat(testObjectA2).isNotNull();
      assertThat(testObjectA2.index()).isEqualTo(1);

      assertThat(testObjectB2).isNotNull();
      assertThat(testObjectB2.index()).isEqualTo(1);
   }

   @Test
   public void testFixtureInjectionWhenSeveralFixturesAndAList(@Fixture ObjectA testObject1,
                                                               @Fixture(count = 3) List<ObjectA> testObjects,
                                                               @Fixture ObjectA testObject2) {
      assertThat(testObject1).isNotNull();
      assertThat(testObject1.index()).isEqualTo(0);

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).extracting(ObjectA::index).containsExactly(0, 1, 2);

      assertThat(testObject2).isNotNull();
      assertThat(testObject2.index()).isEqualTo(1);
   }

   /**
    * Test that, when several lists are injected, there's no stateful index count as for non-list injections.
    * So index is reset to 0 for each new list, even if the fixture type is the same.
    */
   @Test
   public void testFixtureInjectionWhenSeveralLists(@Fixture(count = 2) List<ObjectA> testObjects1,
                                                    @Fixture(count = 2) List<ObjectA> testObjects2) {
      assertThat(testObjects1).extracting(ObjectA::index).containsExactly(0, 1);
      assertThat(testObjects2).extracting(ObjectA::index).containsExactly(0, 1);
   }

   @Test
   public void testBuilderFixtureInjectionWhenNominal(@Fixture ObjectCBuilder testObject) {
      assertThat(testObject).isNotNull();
   }

   @Test
   public void testBuilderFixtureInjectionWhenList(@Fixture List<ObjectCBuilder> testObjects) {
      assertThat(testObjects).isNotNull();
      assertThat(testObjects).map(ObjectCBuilder::build).extracting(ObjectC::index).containsExactly(0);
   }

   @Test
   public void testBuilderFixtureInjectionWhenBuildObject(@Fixture ObjectC testObject) {
      assertThat(testObject).isNotNull();
   }

   @Test
   public void testBuilderFixtureInjectionWhenBuildObjectList(@Fixture List<ObjectC> testObjects) {
      assertThat(testObjects).isNotNull();
      assertThat(testObjects).extracting(ObjectC::index).containsExactly(0);
   }

   public static class CustomList<T> extends AbstractList<T> {

      private final List<T> backingList = new ArrayList<>();

      @Override
      public T get(int index) {
         return backingList.get(index);
      }

      @Override
      public T set(int index, T element) {
         return backingList.set(index, element);
      }

      @Override
      public int size() {
         return backingList.size();
      }
   }

   /**
    * Junit platform internal tester. This enables to test extension internals (parameter injection, ...).
    *
    * @param testMethod test method selector
    * @param exceptionType failure exception type
    * @param exceptionMessage failure exception message
    */
   private void assertThatExtensionFailed(MethodSelector testMethod,
                                          Class<? extends JUnitException> exceptionType,
                                          String exceptionMessage) {
      EngineTestKit
            .engine("junit-jupiter")
            .selectors(testMethod)
            .execute()
            .testEvents()
            .assertThatEvents()
            .filteredOn(Event.byType(EventType.FINISHED))
            .hasSize(1)
            .satisfies(event -> {
               assertThat(event.getPayload()).hasValueSatisfying(payload -> {
                  assertThat(payload).asInstanceOf(type(TestExecutionResult.class)).satisfies(result -> {
                     assertThat(result.getThrowable()).hasValueSatisfying(throwable -> {
                        assertThat(throwable).asInstanceOf(type(exceptionType)).satisfies(exception -> {
                           assertThat(exception.getMessage()).isEqualTo(exceptionMessage);
                        });
                     });
                  });
               });
            }, Index.atIndex(0));
   }

}