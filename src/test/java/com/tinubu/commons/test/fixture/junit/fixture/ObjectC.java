package com.tinubu.commons.test.fixture.junit.fixture;

import com.tinubu.commons.lang.model.AdvancedBuilder;

public class ObjectC {

   private int index;

   public ObjectC(int index) {
      if (index > 99) throw new IndexOutOfBoundsException("Index out of range: " + index);
      this.index = index;
   }

   public int index() {
      return index;
   }

   public static class ObjectCBuilder implements AdvancedBuilder<ObjectC> {

      private int index = 0;

      public ObjectCBuilder index(int index) {
         this.index = index;
         return this;
      }

      @Override
      public ObjectC buildObject() {
         return new ObjectC(index);
      }

   }

}
